#!/bin/bash
#=======================================================================================
#title          :FAIRbyDesign docs
#description    :FAIR by Design / FAIRDS Documentation
#author         :Bart Nijsse & Jasper Koehorst
#date           :2022
#version        :0.0.1
#=======================================================================================

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

# Build book
cd $DIR
jupyter-book clean . && jupyter-book build .