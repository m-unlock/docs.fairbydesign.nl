#!/bin/bash
#=======================================================================================
#title          :FAIRbyDesign docs build image
#description    :Build container for unlock documentation
#author         :Bart Nijsse & Jasper Koehorst
#date           :2023
#version        :0.0.1
#=======================================================================================

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"

docker login registry.gitlab.com

docker build -t registry.gitlab.com/m-unlock/docs.fairbydesign.nl $DIR

docker push registry.gitlab.com/m-unlock/docs.fairbydesign.nl