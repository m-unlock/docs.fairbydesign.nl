# Frequently Asked Questions

## Validation
  - **Can the FAIR data station interact with other validation libraries such as the [ELIXIR Biovalidator](https://doi.org/10.1093/bioinformatics/btac195)?** <br>
  For advanced users the FAIR-DS can export the FAIR-DS metadata file into a multitude of JSON files (one for each package) which makes it possible to use in addition to the FAIR-DS  other validation libraries (such as the ELIXIR Biovalidator). The JSON package files can be found in the git repository.
  <br> <br>
  - **Is there a mechanism for users to add a new regular expression?** <br>
  Field–value pairs, value specifications, examples and regular expressions are stored in an external metadata file in an open excel format. 1) When users use their own local instance of the FAIR Data Station, this template file can be modified by the user to add new or update existing regular expressions, to add new fields and to add or replace packages. 
  <br> <br>
  - **Where do the checklists and regular expressions come from?** <br>
  The checklists are directly obtained from [https://www.ebi.ac.uk/ena/browser/checklists](https://www.ebi.ac.uk/ena/browser/)  checklists and the ENA supplied regular expressions are used when the field format is restricted. The regular expression for DOI was obtained from [https://registry.identifiers.org/registry/doi](https://registry.identifiers.org/registry/doi).
  <br> <br>
  - **Is there a way to validate ontology terms against the relevant ontologies?** <br>
  We are currently exploring the validation of ontology terms. The FAIR-DS metadata file (metadata.xlsx) regular expression field now also accepts an URL-link to an OWL-file. During start-up of the app, the OWL-file is retrieved and transformed into a RDF database. During the validation process the user-recorded terms are checked against rdfs:label values of the selected ontology. As a working example we have implemented a first version of term validation of the Environment Ontology ([http://purl.obolibrary.org/obo/envo.owl](http://purl.obolibrary.org/obo/envo.owl)).


## ISA alignment
  - **The FAIR-DS uses an amended version of the original three-tier Investigation, Study, Assay (ISA) metadata framework [https://isa-tools.org](https://isa-tools.org). What changes were implemented and how does it affect metadata database searches?** <br>
  During the development of the tool, we noted that for most experimentalists/data producers “source material” and “sample material” refer to the same thing. We therefore implemented the object types "Observation unit", used in the MIAPPE v1.1 ISA-Tab format ([https://github.com/MIAPPE/ISA-Tab-for-plant-phenotyping](https://github.com/MIAPPE/ISA-Tab-for-plant-phenotyping)) and Sample from the Just Enough Results Model [https://jermontology.org](https://jermontology.org). From the perception of an experimentalist / data producer this separation make sense as the minimal information models used, focus on static and dynamic contextual data of the sampling environment. However in the app the schema is aligned with the current RDF dataset shown here: [https://github.com/ISA-tools/isa-api/blob/master/isa-cookbook/content/notebooks/ISA-jsonld-basic-test.ttl](https://github.com/ISA-tools/isa-api/blob/master/isa-cookbook/content/notebooks/ISA-jsonld-basic-test.ttl) by mapping “Observation unit” and “Sample” to “source’ and “sample” -material ([example.ttl](https://gitlab.com/m-unlock/fairds/-/blob/745ce49c0915d23111dd6050694ccc021332c368/examples/example.ttl)).

## Metadata
 - **I messed up my metadata template file what now?** <br>
 Remove the excel file from the folder and restart the application. A new file with default settings will be automatically generated.
 <br> <br>
 - **I want to use the metadata template developed by my colleague** <br>
 Make sure it complies with our format and simply replace the file (and store your own copy in a different folder) and restart the app.