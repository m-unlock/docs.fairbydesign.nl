# Intro

The FAIR Data Station is a web application that allows you to create and manage metadata in a FAIR manner. The application is based on the ISA metadata framework and uses the ISA structure to generate RDF datasets. 

## Metadata Configurator

The metadata configurator allows you to create a metadata template in a FAIR manner. The different levels implemented are Investigation, Study, Observation Unit, Sample, Assay.

## Validate Metadata

After creating a metadata template you can validate your metadata against the template using the FAIR Data Station engine. After succesful validation an RDF dataset is generated that can be used for more automated downstream processs and analysis.

## ENA Submission

To publish your data in the ENA you can use the FAIR Data Station to generate the required metadata and data files. The generated files  (XML format)can be uploaded to the ENA submission portal.

## ENA Export

The European Nucleotide Archive (ENA) is a repository for raw data from sequencing experiments. The ENA Export functionality allows you to download the metadata from the ENA in a FAIR manner. The metadata can be downloaded and structured into the Excel template files that you are able to generate using the Metada Configurator.

## Terms Overview

The Terms Overview allows you to browse the terms used in the FAIR Data Station. The terms are grouped by type and can be searched using the search boxes available.

## Browser

The browser allows you to browse the data available in the iRODS or Yoda data storage system. In Yoda there is also the option to reorganise your data using the metadata template.

## Disk Usage

To gain more insights into how much storage you are consuming you can go to the Disk Usage overview. This overview shows you the total amount of storage used in the iRODS and Yoda data storage systems.