# Setup your own

## Running the application

To quickly get something running download the (latest) version of the application from [http://download.systemsbiology.nl/unlock/](http://download.systemsbiology.nl/unlock/) and download the fairds-latest.jar file.

To start the program in a terminal (Linux/Mac) or Command Prompt/Powershell in Windows by typing: `java -jar fairds-latest.jar`

When you see the following line, the application has started...

`Started Application in 3.666 seconds (process running for 6.147)`

Now you can access the application using your browser at [http://localhost:8083](http://localhost:8083)

(The application is now only accessible from your own device)

Here you can generate your templates and validate your metadata. Or you can work on your own metadata packages, see: [Templates](./templates.md)

### Using docker

When you want to use the application through docker the following command can be used:

**Mac:**
> docker run -v `$PWD`/fairds_storage:/fairds_storage -it -p 8083:8083 docker-registry.wur.nl/m-unlock/docker/fairds:latest

**Linux:**
> docker run -v `$PWD`/fairds_storage:/fairds_storage -it -p 8083:8083 docker-registry.wur.nl/m-unlock/docker/fairds:latest

**Windows:**
> docker run -v "%cd%"/fairds_storage:/fairds_storage -it -p 8083:8083 docker-registry.wur.nl/m-unlock/docker/fairds:latest

This command will mount the current directory into the fairds_storage folder inside the docker to ensure that all files are stored locally.