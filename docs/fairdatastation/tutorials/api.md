# API

The FAIR Data Station also has an API function. At the moment only the validator is available through the API.

## Validator

To validate an excel file you can use the following command:


```bash
curl -X POST http://localhost:8083/api/upload -F "file=@/Users/koeho006/Downloads/ValidationDemo.xlsx"
```

This is used through the bash environment but it supports any programming language that can post and receive data.

It will either return the RDF file or a validatione error:

```bash
curl -X POST http://localhost:8083/api/upload -F "file=@/Users/koeho006/Downloads/ValidationDemo.xlsx"
 does not match the pattern of (female|hermaphrodite|male|neuter|not applicable|not collected|not provided|other|restricted access) regex (female|hermaphrodite|male|neuter|not applicable|not collected|not provided|other|restricted access) such as in example "female"
(female|hermaphrodite|male|neuter|not applicable|not collected|not provided|other|restricted access)
femalee% 
```