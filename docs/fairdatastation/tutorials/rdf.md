# RDF Tutorial

The metadata is converted to an [RDF](https://en.wikipedia.org/wiki/Resource_Description_Framework) data file and can be queried using the [SPARQL](https://en.wikipedia.org/wiki/SPARQL) query language.

## Loading the data

We will load the dataset from the previous tutorial into GraphDB. To install Graphdb, go to [graphdb-free](https://www.ontotext.com/products/graphdb/graphdb-free/) and register for an installation (registration is free and required!).

After successful installation you should be able to access the application at [http://localhost:7200](http://localhost:7200). To load the data into a database do the following:

If you did not follow the tutorial you can obtain the resulting RDF file from [here](https://gitlab.com/m-unlock/docs.fairbydesign.nl/-/raw/main/docs/fairdatastation/data/ValidationDemoFinished.ttl) and the complete study in RDF with all participants [here](https://gitlab.com/m-unlock/docs.fairbydesign.nl/-/raw/main/docs/fairdatastation/data/ValidationDemoComplete.ttl)

- Click on repositories
- Create new repository
  - GraphDB Repository
- Give it a name in the Repository ID*
  - Leave everything else as default
- Top right choose repository change to the one you just created
- Import
- Import the RDF file
```{tip}
  We suggest importing RDF data into separate named graphs (contexts).  
  This allows for the selective removal and tracking of triples by deleting graphs instead of wiping the repository.  
  The approach is done using the **Target graphs** option during import,  
  select **Named graph** and type something meanigfull as a IRI, e.g. `file:my-datafile.ttl` for instance.  
  Manage graphs via the *Explore -> Graphs Overview* or *SPARQL UPDATE* queries.  
```
- Click import
- The data should be loaded within a few minutes

When you go back to the home screen (click GraphDB) you should see your local active repostiroy having a total of 631 statements.

### Enable autocomplete

To make life easier we will enable autocomplete for the SPARQL queries. To do this do the following:

- Click on the repository you just created
- Click on settings
- Click on autocomplete
- Click on enable autocomplete

### Explore the data

Now we have the data loaded we can start exploring the data. To do this we will use the Explore option first. To do this do the following:

- Click on the repository you just created
- Click on explore
- Visual graph
- In the Easy Graph bar type "Project" and select the [http://jermontology.org/ontology/JERMOntology#Project](http://jermontology.org/ontology/JERMOntology#Project) URL.
- Click on the "prj_HIV-Ghana" node
- Follow the "hasPart" links to other nodes

Do you see the connections between the nodes and the excel sheet?

- Click on one of the Observation Units (e.g., obs_XDRS176892)
- A sidebar should appear
- What properties are used and do you see different namespaces?

### Query the data

Now that we have explored the data we can start querying it.
To do this we will use the SPARQL query language:

- Click on the repository you just created
- Click on SPARQL
- In the query box type the following query:

**To obtain all observation units**

```SPARQL
PREFIX jerm: <http://jermontology.org/ontology/JERMOntology#>
PREFIX ppeo: <http://purl.org/ppeo/PPEO.owl#>
SELECT *
WHERE {
  ?ou a ppeo:observation_unit .
}
```

| Index | Observation ID                                      |
|-------|-----------------------------------------------------|
| 1     | fair:inv_demo1/stu_control_vs_infected/obs_O_SAMD00665829 |
| 2     | fair:inv_demo1/stu_control_vs_infected/obs_O_SAMD00665819 |
| 3     | fair:inv_demo1/stu_control_vs_infected/obs_O_SAMD00665820 |
| 4     | fair:inv_demo1/stu_control_vs_infected/obs_O_SAMD00665831 |

**To obtain all observation units that are female**

```SPARQL
PREFIX jerm: <http://jermontology.org/ontology/JERMOntology#>
PREFIX ppeo: <http://purl.org/ppeo/PPEO.owl#>
PREFIX mixs: <https://w3id.org/mixs/>
SELECT *
WHERE {
  ?ou a ppeo:observation_unit .
  ?ou mixs:0000811 'female' .
}
```

|  | Observation ID                                           |
|-------|----------------------------------------------------------|
| 1     | fair:inv_demo1/stu_control_vs_infected/obs_O_SAMD00665829 |
| 2     | fair:inv_demo1/stu_control_vs_infected/obs_O_SAMD00665819 |
| 3     | fair:inv_demo1/stu_control_vs_infected/obs_O_SAMD00665820 |
| 4     | fair:inv_demo1/stu_control_vs_infected/obs_O_SAMD00665831 |

**To obtain all observation units that are female and a trader**

```SPARQL
PREFIX jerm: <http://jermontology.org/ontology/JERMOntology#>
PREFIX ppeo: <http://purl.org/ppeo/PPEO.owl#>
PREFIX mixs: <https://w3id.org/mixs/>
PREFIX fair: <http://fairbydesign.nl/ontology/>
SELECT *
WHERE {
  ?ou a ppeo:observation_unit .
  ?ou mixs:0000811 'female' .
  ?ou jerm:hasPart ?sample .
  ?sample fair:occupation 'Trader' .
}
```

|| ou | sample |
|-|-----|--------|
|1|fair:inv_demo1/stu_control_vs_infected/obs_O_SAMD00665820|fair:inv_demo1/stu_control_vs_infected/obs_O_SAMD00665820/sam_SAMD00665820|

**Overview of the different occupations**

```SPARQL
PREFIX jerm: <http://jermontology.org/ontology/JERMOntology#>
PREFIX ppeo: <http://purl.org/ppeo/PPEO.owl#>
PREFIX mixs: <https://w3id.org/mixs/>
PREFIX fair: <http://fairbydesign.nl/ontology/>
SELECT DISTINCT ?occ
WHERE {
  ?ou a ppeo:observation_unit .
  ?ou jerm:hasPart/fair:occupation ?occ .
}
```

| |occ|
|-|-|
|1| "Unemployed"|
|2| "Farming"|
|3| "Trader"|
|4| "Farmer"|

As you might see the SPARQL query language is for variables case sensitive and making it crucial to have proper standardisation methods in place. For example there is no 'trader' in the dataset but there is a 'Trader'.
