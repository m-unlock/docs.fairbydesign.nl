# FAIR-DS Local & Metadata

This application is written in java and if you have java installed on your computer you should be able to start the program on your local machine.

- To obtain the program go to [http://download.systemsbiology.nl/unlock](http://download.systemsbiology.nl/unlock/?C=M;O=D) and download the fairds-latest.jar to your computer
- To start the program start a command line interface (Prompt, Shell, Terminal depending on your operating system).
  - For mac: Open the terminal app
  - For ubuntu: Open the terminal app
  - For windows: Open the Command Prompt app
- Type: java -jar fairds-latest.jar (When you are in the same folder as the jar file)
- When you see the following line, the application has started... ***Tomcat started on port(s): 8083 ...***.
- Now you can access the application using your browser at [http://localhost:8083](http://localhost:8083).
