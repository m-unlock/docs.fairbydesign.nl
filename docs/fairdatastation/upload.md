# Validation

To validate your excel file you can upload it at [https://fairds.fairbydesign.nl/validate](https://fairds.fairbydesign.nl/validate). 
This will validate your file and will show you if there are any inconsistencies. 

![](../figures/upload.png)

After validation and if you filled in everything correctly you should see a report summary of the validation process.
In addition you can download the metadata file as RDF.

If the validation was unsuccesful please check the error message carfully and see if you can identify the problem. 
If not feel free to contact us to help you out.

## Validation report

During validation the metadata file is checked for inconsistencies. When the validation is successful you will see a summary of the validation process.

Such a summary can look like this:

```
Analysing investigation information
Analysing study information
Analyzing observation unit sheet
Finished processing Sample - miscellaneous natural  sheet
Processing Assay - Illumina sheet
Finished parsing Assay - Illumina sheet
Processing Assay - Amplicon demultiplexed sheet
Finished parsing Assay - Amplicon demultiplexed sheet
Found Lat long values to convert to the GeoSPARQL format.
Validating RDF file
RDF file passed validation
Validation successful, user not logged in.
Result file not uploaded to the data storage facility
Validation appeared to be successful.
```

## Validation errors

However if the validation was not successful you will see a summary of the validation process and an error message.

Such a summary can look like this:

```
=================================================================

Your dataset did not pass the validation...

When it is unclear please provide the following message to your data steward

Analysing investigation information
Analysing study information
Analyzing observation unit sheet
The value "malee" of "sex" in the "ObservationUnit" sheet does not match the pattern of (female|hermaphrodite|male|neuter|not applicable|not collected|not provided|other|restricted access) regex (female|hermaphrodite|male|neuter|not applicable|not collected|not provided|other|restricted access) such as in ex.getTerm()ample "female"

Please validate your mappings in the excel file

=================================================================
```

The error message will give you a hint on what went wrong. In this case `The value "malee" of "sex" in the "ObservationUnit" sheet does not match the pattern of (female|hermaphrodite|male|neuter|not applicable|not collected|not provided|other|restricted access)`, which means that the value `malee` is not a valid value for the `sex` column in the `ObservationUnit` sheet.

To correct this you can change the value to one of the valid values, in this case to `male`. If no new errors are found your metadata file should pass the validation.

## Download RDF

During the validation process the excel file is converted to RDF. This RDF file can be downloaded by clicking on the `Download RDF` button. This file can be used in different [Triple Stores](https://en.wikipedia.org/wiki/Triplestore). The stores can be used to query the data and to make it available for other applications.